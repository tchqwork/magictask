﻿using UnityEngine;

namespace MagicTask
{
    /// <summary>
    /// Конфиг для уровней.
    /// </summary>
    [CreateAssetMenu(fileName = "New Level Data Preset", menuName = "LevelPresets")]
    public class LevelDataPreset : ScriptableObject
    {
        public int Id;
        public string Name;
        /// <summary>
        /// Картинка для названия уровня.
        /// Либо я чего-то не знаю, либо обводка в TMP не поддерживает градиент.
        /// Поэтому картинка.
        /// </summary>
        public Sprite NameSprite;
        public Sprite Picture;
        public int LevelsTotal;
    }
}