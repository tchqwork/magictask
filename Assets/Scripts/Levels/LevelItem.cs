﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace MagicTask
{
    /// <summary>
    /// Элемент списка уровней.
    /// </summary>
    public class LevelItem : MonoBehaviour
    {
        private int _id;
        [SerializeField] private TextMeshProUGUI _nameText;
        [SerializeField] private Image _nameImage;
        [SerializeField] private Image _picture;
        [SerializeField] private TextMeshProUGUI _levelsPassedlText;
        [SerializeField] private TextMeshProUGUI _levelsTotalText;

        public Action<int> OnLevelSelected;

        public void Init(int id, string name, Sprite nameSprite, Sprite picture, int levelsPassed, int levelsTotal)
        {
            _id = id;
            _nameText.text = name;
            _nameImage.sprite = nameSprite;
            _picture.sprite = picture;
            _levelsPassedlText.text = levelsPassed.ToString();
            _levelsTotalText.text = levelsTotal.ToString();
            //_statisticsText.text = $"<sup>{levelsPassed}</sup>/<sub>{levelsTotal}</sub>";
        }

        public void SelectLevel()
        {
            OnLevelSelected?.Invoke(_id);
        }
    }
}
