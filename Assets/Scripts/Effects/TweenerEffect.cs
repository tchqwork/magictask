﻿using DG.Tweening;

namespace MagicTask
{
    /// <summary>
    /// Расширение класса анимации, основанное на использовании библиотеки DOTween.
    /// </summary>
    public abstract class TweenerEffect : AnimationEffect
    {
        protected Tweener tweener;

        protected bool _isDone = false;
        override public bool IsDone { get { return _isDone; } }
        override public bool IsPlaying { get { return (tweener != null && tweener.IsActive()) ? tweener.IsPlaying() : false; } }
        override public void Stop()
        {
            tweener?.Kill(false);
            tweener = null;
            _isDone = false;
        }
    }
}
