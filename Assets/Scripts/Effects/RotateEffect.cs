﻿using DG.Tweening;
using UnityEngine;

namespace MagicTask
{
    /// <summary>
    /// Анимация вращения.
    /// </summary>
    public class RotateEffect : TweenerEffect
    {
        override public void Play()
        {
            if (tweener.IsActive() && tweener.IsPlaying())
            {
                return;
            }
            _isDone = false;
            tweener = transform.DORotate(new Vector3(0, 0, -360), 3f, RotateMode.FastBeyond360)
                .SetRelative(true)
                .SetEase(Ease.Linear)
                .SetLoops(int.MaxValue); // Если поставить -1, то не сработает OnComplete и объект сам в исходное состояние не вернётся.
        }

        override public void Stop()
        {
            tweener?.Kill(true);
            tweener = null;
            _isDone = false;
        }
    }
}
