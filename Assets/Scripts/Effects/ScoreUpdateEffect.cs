﻿using DG.Tweening;
using TMPro;
using UnityEngine;

namespace MagicTask
{
    /// <summary>
    /// Эффект начисления очков.
    /// </summary>
    public class ScoreUpdateEffect : TweenerEffect
    {
        [SerializeField] private TextMeshProUGUI scoreText;
        [SerializeField] private float scaleFrom = 1f;
        [SerializeField] private float scaleTo = 1.2f;
        /// <summary>
        /// Общее время выполнения анимации.
        /// </summary>
        [SerializeField] private float duration = 1f;

        private int _fromScore;
        private int _toScore;

        public void Set(int from, int to)
        {
            _fromScore = from;
            _toScore = to;
        }

        override public void Play()
        {
            // Если запустить эффект раньше, чем он отсчитает нужное количество,
            // то просто произойдёт перескок на новое базовое значение и оттуда будет продолжен отсчёт.
            if (tweener.IsActive() && tweener.IsPlaying())
            {
                tweener.Kill(false);
            }

            _isDone = false;
            transform.localScale = Vector3.one;
            int steps = _toScore - _fromScore;
            _fromScore++;
            scoreText.text = _fromScore.ToString();

            tweener = transform.DOScale(scaleTo, duration / steps)
                .SetLoops(steps)
                .SetEase(Ease.OutBack)
                .OnStepComplete(() =>
                {
                    if (tweener.CompletedLoops() != tweener.Loops())
                    {
                        _fromScore++;
                        scoreText.text = _fromScore.ToString();
                    }
                })
                .OnComplete(() =>
                {
                    transform.localScale = Vector3.one;
                    _isDone = true;
                    OnCompleted?.Invoke();
            });
        }
    }
}