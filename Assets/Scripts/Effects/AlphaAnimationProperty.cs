﻿using UnityEngine;

namespace MagicTask
{
    /// <summary>
    /// Класс, позволяющий менять прозрачность всем материалам, добавленным в список.
    /// Смысл его в том, чтобы можно было легко получить доступ к изменению сразу всех материалов, непосредственно в анимационном клипе.
    /// </summary>
    public class AlphaAnimationProperty : MonoBehaviour
    {
        private float _alpha = -1;
        [Range(0, 1)]
        public float Alpha;

        [SerializeField] private Renderer[] renderers;
        private bool hasMaterials = false;

        private void Awake()
        {
            if (renderers != null && renderers.Length > 0)
            {
                hasMaterials = true;
                if (_alpha != Alpha)
                {
                    _alpha = Alpha;
                    SetAlpha(_alpha);
                }
            }
        }
        private void SetAlpha(float x)
        {
            foreach (var r in renderers)
            {
                r.material.SetFloat("_alpha", x);
            }
        }

        private void Update()
        {
            if (hasMaterials && _alpha != Alpha)
            {
                _alpha = Alpha;
                SetAlpha(_alpha);
            }
        }
    }
}