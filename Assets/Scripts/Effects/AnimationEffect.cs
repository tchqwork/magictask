﻿using System;
using UnityEngine;

namespace MagicTask
{
    /// <summary>
    /// Базовый класс для эффектов анимации, которые прикрепляются к игровым объектам.
    /// </summary>
    public abstract class AnimationEffect : MonoBehaviour
    {
        public abstract bool IsDone { get; }
        public abstract bool IsPlaying { get; }
        public abstract void Play();
        public abstract void Stop();

        public Action OnCompleted;
    }
}
