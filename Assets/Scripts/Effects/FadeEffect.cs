﻿using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace MagicTask
{
    /// <summary>
    /// Эффект плавного появления картинки.
    /// </summary>
    public class FadeEffect : TweenerEffect
    {
        [SerializeField] private Image _image;

        override public void Play()
        {
            if (tweener.IsActive() && tweener.IsPlaying())
            {
                return;
            }

            _isDone = false;
            var c = _image.color;
            c.a = 0;
            _image.color = c;

            tweener = _image.DOFade(1, 1f)
                .OnComplete(() =>
                {
                    _isDone = true;
                });
        }

        public override void Stop()
        {
            base.Stop();
            var c = _image.color;
            c.a = 0;
            _image.color = c;
        }
    }
}
