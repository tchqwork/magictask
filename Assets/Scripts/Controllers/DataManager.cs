﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace MagicTask
{
    /// <summary>
    /// Класс-заглушка для получения данных.
    /// </summary>
    public class DataManager : SingletonBehaviour<DataManager>
    {
        public int GetLastScore()
        {
            return 57;
        }
        /// <summary>
        /// Получаем откуда-то данные об уровнях
        /// </summary>
        public List<KeyValuePair<int,int>> GetLevelsData()
        {
            return new List<KeyValuePair<int, int>> {
                new KeyValuePair<int, int>(1,10),
                new KeyValuePair<int, int>(2,3),
                new KeyValuePair<int, int>(3,5),
                new KeyValuePair<int, int>(4,10),
                new KeyValuePair<int, int>(5,2),
            };
        }
    }
}