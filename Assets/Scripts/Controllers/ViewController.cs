﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

namespace MagicTask
{
    /// <summary>
    /// Основной контроллер для управления UI.
    /// </summary>
    public class ViewController : MonoBehaviour
    {
        /// <summary>
        /// Префаб для блока с информацией в списке уровней.
        /// </summary>
        [SerializeField] private GameObject _infoPrefab;

        [Header("Score")]
        [SerializeField] private AnimationEffect _starAnimation;
        [SerializeField] private ScoreUpdateEffect _scoreAnimation;

        [Header("Scroll buttons")]
        [SerializeField] private AnimationEffect _leftButtonAnimation;
        [SerializeField] private AnimationEffect _rightButtonAnimation;
        /// <summary>
        /// Отступ, после котрого кнопки буду появляться от края (в "процентах" от скролла)
        /// </summary>
        [Range(0.1f,0.5f)]
        [SerializeField] private float _showButtonDeltaScroll = 0.1f;

        [Header("Levels")]
        [SerializeField] private LevelItem _levelItemPrefab;
        [SerializeField] private ScrollRect _scrollView;
        [SerializeField] private Transform _contentContainer;
        [SerializeField] private List<LevelDataPreset> _levelPresets;

        private int _currentScore = 57;

        private void Awake()
        {
            _scoreAnimation.OnCompleted += _starAnimation.Stop;
            _scrollView.onValueChanged.AddListener(UpdateScrollButtonsVisibility);
        }

        // Просто тест.
        IEnumerator Start()
        {
            _currentScore = DataManager.Instance.GetLastScore();
            PopulateLevelsList(DataManager.Instance.GetLevelsData());
            
            yield return new WaitForSeconds(1f);
            AddScore(5);
            yield return new WaitForSeconds(1f);
            AddScore(5);
            yield return new WaitForSeconds(5f);
            AddScore(5);
            yield return null;
        }

        /// <summary>
        /// Заполнение списка уровенй
        /// </summary>
        private void PopulateLevelsList(List<KeyValuePair<int, int>> levelsProgress)
        {
            for (int i = 0; i < _contentContainer.childCount; i++)
            {
                Destroy(_contentContainer.GetChild(i).gameObject);
            }
            foreach (var level in _levelPresets)
            {
                var item = Instantiate(_levelItemPrefab, _contentContainer);
                var levelProgress = levelsProgress.FirstOrDefault(x => x.Key == level.Id);
                item.Init(level.Id, level.Name, level.NameSprite, level.Picture, levelProgress.Value, level.LevelsTotal);
                item.OnLevelSelected += SelectLevel;
            }
            Instantiate(_infoPrefab, _contentContainer);
            _scrollView.horizontalNormalizedPosition = 0;
        }

        public void SelectLevel(int id)
        {
            Debug.Log($"Selected level id = {id}");
        }

        public void AddScore(int score)
        {
            Debug.Log($"Add score = {score}");
            _starAnimation.Play();
            _scoreAnimation.Set(_currentScore, _currentScore + score);
            _scoreAnimation.Play();
            _currentScore += score;
        }
        /// <summary>
        /// Проверяем, нужно ли показывать кнопки скролла уровней.
        /// </summary>
        /// <param name="val"></param>
        private void UpdateScrollButtonsVisibility(Vector2 val)
        {
            if (val.x > _showButtonDeltaScroll)
            {
                if (!_leftButtonAnimation.IsDone && !_leftButtonAnimation.IsPlaying)
                {
                    _leftButtonAnimation.Play();
                }
            }
            if (val.x < 1f - _showButtonDeltaScroll)
            {
                if (!_rightButtonAnimation.IsDone && !_rightButtonAnimation.IsPlaying)
                {
                    _rightButtonAnimation.Play();
                }
            }
        }
    }
}