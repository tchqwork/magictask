﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace MagicTask
{
    /// <summary>
    /// Кнопка для скролла по нажатию.
    /// </summary>
    public class ScrollButtonBehaviour : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {
        [SerializeField] private ScrollRect _scrollView;
        [SerializeField] private ScrollType _type;
        [SerializeField] private float _speed;
        [SerializeField] UnityEvent OnStartReached;
        [SerializeField] UnityEvent OnEndReached;

        private bool pressed = false;

        void Awake()
        {
            _scrollView.onValueChanged.AddListener(CheckValue);
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            pressed = true;
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            pressed = false;
        }

        public void SetScrollValue(float value)
        {
            _scrollView.horizontalNormalizedPosition = Mathf.Clamp(value, 0f, 1f);
        }

        void Update()
        {
            if (pressed)
            {
                _scrollView.horizontalNormalizedPosition = Mathf.Clamp(_scrollView.horizontalNormalizedPosition + _speed * Time.deltaTime, 0f, 1f);
            }
        }

        private void CheckValue(Vector2 value)
        {
            Debug.Log(value);
            if ((_type == ScrollType.Horizontal && value.x <= 0.05f)
             || (_type == ScrollType.Vertical && value.y <= 0.05f))
            {
                Debug.Log("Start Reached");
                OnStartReached?.Invoke();
            }
            if ((_type == ScrollType.Horizontal && value.x >= 0.95f)
                  || (_type == ScrollType.Vertical && value.y >= 0.95f))
            {
                Debug.Log("End Reached");
                OnEndReached?.Invoke();
            }
        }

        public enum ScrollType
        {
            Horizontal,
            Vertical
        }
    }
}