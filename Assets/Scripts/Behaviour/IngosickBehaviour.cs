﻿using UnityEngine;

namespace MagicTask
{
    /// <summary>
    /// Класс, реализующий поведение персонажа в AR сцене.
    /// </summary>
    public class IngosickBehaviour : MonoBehaviour
    {
        AudioSource audioSource;

        [Header("Sound Clips")]
        [SerializeField] AudioClip AppearSound;
        [SerializeField] AudioClip StandSound;

        private void Awake()
        {
            audioSource = GetComponent<AudioSource>();
        }

        protected void Appear()
        {
            audioSource.clip = AppearSound;
            audioSource.Play();
        }

        protected void Dance()
        {
            audioSource.clip = StandSound;
            audioSource.Play();
        }

        protected void Hide()
        {

        }
    }
}